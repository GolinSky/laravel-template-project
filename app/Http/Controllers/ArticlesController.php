<?php

namespace App\Http\Controllers;

use App\Article;
use App\Tag;
use App\User;

class ArticlesController extends Controller
{
    //todo: Make a interface for controller for empty methods which are located down

    //Show a single resource
    public function show(Article $article)
    {
        return view('articles.show', ['article' => $article]);
    }

    // Render list of resource
    public function index()
    {
        if(request('tag'))
        {
            $articles = Tag::where('name', request('tag'))->firstOrFail()->articles;
            return view('articles.index',[
                'articles' => $articles
            ]);
        }
        return view('articles.index',[
        'articles' => Article::latest()->get()
    ]);
    }

    //Show a view to create a new resource
    public function create()
    {
        return view('articles.create', [
            'tags' => Tag::all()
        ]);
    }

    //Persist the new resource
    public function store()
    {
        //persist the new article
        $this->getValidateRequest();
        $article = new Article(request(['tittle', 'excerpt', 'body']));
        $article->user_id = User::first()->id;
        $article->save();
        if(request()->has('tags'))
        {
            $article->tags()->attach(request('tags'));
        }

        return redirect(route('articles.index'));
    }

    //Show a view to edit an existing resource
    public function edit(Article $article)
    {
        return view('articles.edit',['article' => $article]);
    }

    //Persist the edited resource
    public function update(Article $article)
    {
        $article->update($this->getValidateRequest());
        return redirect($article->path());
    }

    //Delete the resource
    public function destroy()
    {

    }

    /**
     * @return mixed
     */
    private function getValidateRequest()
    {
        return request()->validate([
            'tittle' => 'required',
            'excerpt' => 'required',
            'body' => 'required',
            'tags' => 'exists:tags,id' //check for tag id is existing
        ]);
    }


}
