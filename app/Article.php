<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Article extends Model
{
    protected $fillable = ['tittle', 'excerpt', 'body'];

    public function path()
    {
        return route('articles.show', $this);
    }

    public function user()// using anonymous shit (name_method) for creation 'user_id' param
    {
        return $this->belongsTo(User::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }
}


