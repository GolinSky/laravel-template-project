<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assigmment extends Model
{
    public function complete()
    {
        $this->completed = true;
        $this->save();
    }

    public function resume()
    {
        $this->completed = false;
        $this->save();
    }
}
