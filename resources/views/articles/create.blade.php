@extends('layout4sw')

    @section('head')
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">

    @endsection
@section('content')
    <div id="wrapper">
        <div id="page" class="container">
            <h1 class="heading has-text-weight-bold is-size-4">New Article</h1>
        </div>
    </div>

    <form method="post" action ="/articles">
        @csrf
        <label class="label" for="tittle">Tittle</label>
        <div class="control">
            <input class="input @error('tittle') is-danger @enderror" type="text" name="tittle" id="tittle" required>
           @error('tittle'))
            <p class="help is-danger">{{ $errors->first('tittle') }}</p>
                @enderror
        </div>
        <label class="label" for="excerpt">Excerpt</label>
        <div class="control">
            <textarea class="input" type="text" name="excerpt" id="excerpt" required></textarea>
        </div>
        <label class="label" for="body">Body</label>
        <div class="control">
            <textarea class="input" type="text" name="body" id="body" required></textarea>
        </div>

        <label class="label" for="excerpt">Tags</label>

        <div  class="select is-multiple">
            <select
                name="tags[]"
                multiple
            >
                @foreach($tags as $tag)
                <option value="{{$tag->id}}">{{$tag->name}}</option>
                    @endforeach
            </select>
        </div>
        <div class="field is-grouped">
            <div class="control">
                <button class="button is-link" type="submit">Submit</button>
            </div>
        </div>
    </form>


@stop
