@extends('layout4sw')
@section('head')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">

@endsection
@section('content')
    <div id="wrapper">
        <div id="page" class="container">
            <h1 class="heading has-text-weight-bold is-size-4">Update Article</h1>
        </div>
    </div>

    <form method="POST" action ="/articles/ {{ $article->id }}">
        @csrf
        @method('PUT')
        <label class="label" for="tittle">Tittle</label>
        <div class="control">
            <input class="input" type="text" name="tittle" id="tittle" value="{{ $article->tittle }}">
        </div>
        <label class="label" for="excerpt">Excerpt</label>
        <div class="control">
            <textarea class="input" type="text" name="excerpt" id="excerpt" >{{ $article->excerpt }}</textarea>
        </div>
        <label class="label" for="body">Body</label>
        <div class="control">
            <textarea class="input" type="text" name="body" id="body" >{{ $article->body}}</textarea>
        </div>

        <div class="field is-grouped">
            <div class="control">
                <button class="button is-link" type="submit">Submit</button>
            </div>
        </div>
    </form>
@endsection
