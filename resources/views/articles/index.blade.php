@extends('layout4sw')


@section('content')

    <div id="tittle">
        <a href="{{route('articles.create')}}" class="button">Create an Article</a>
            <br>            <br>
        <br>

    @forelse($articles as $article)

                <a href="{{ $article->path() }}"><h3>{{$article->tittle}}</h3></a>
            <p>{{$article->body}}</p>
            <br>
        @empty
            <p>No relevant articles yet</p>
        @endforelse

    </div>

@endsection
