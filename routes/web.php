<?php


use Illuminate\Support\Facades\Auth;

Route::get('/post/{post}', 'PostsController@show');

Route::get('/contact', function (){
    return view('contact');
});

//route to custom web page
Route::get('/', function (){
    return view('simpleWork');
});


Route::get('/about', function (){
    return view('about',[
        'articles' => App\Article::take(3)->latest()->get()
    ]);
});

Route::get('/articles','ArticlesController@index')->name('articles.index');
Route::post('/articles','ArticlesController@store');
Route::get('/articles/create', 'ArticlesController@create')->name('articles.create');
Route::get('/articles/{article}', 'ArticlesController@show')->name('articles.show');
Route::get('/articles/{article}/edit', 'ArticlesController@edit');
Route::put('/articles/{article}', 'ArticlesController@update');

Auth::routes();

Route::get('/home', 'HomeController@index')
    ->name('home')
    ->middleware('auth');
